const d1 = document.getElementById('d1');

const options = ['rock','paper','scissors']
let choice;

const choosePaper = document.getElementById('paper');
const chooseRock = document.getElementById('rock');
const chooseScissors = document.getElementById('scissors');

const jokenpo = function(event) {
    let outcome;
    const randomChoice = options[Math.floor(Math.random() * options.length)]

    const allChoices = document.querySelectorAll('div');

    // function reset() {
    //     for (let i = 0; i < allChoices.length; i++) {
    //         allChoices[i].style.zIndex = 1;
    //     }
    //  }

    //  reset();

    if (event.target === choosePaper) {
        choice = 'paper';
        outcome = 'draw';
        if (randomChoice === 'scissors') {
            outcome = 'lost';
        }
        if (randomChoice === 'rock') {
            outcome = 'win';
        }
    }
    if (event.target === chooseRock) {
        choice = 'rock';
        outcome = 'draw';
        if (randomChoice === 'paper') {
            outcome = 'lost';
        }
        if (randomChoice === 'scissors') {
            outcome = 'win';
        }
    }
    if (event.target === chooseScissors) {
        choice = 'scissors';
        outcome = 'draw';
        if (randomChoice === 'rock') {
            outcome = 'lost';
        }
        if (randomChoice === 'paper') {
            outcome = 'win';
        }
    }

    console.log(choice);
    console.log(randomChoice);
    console.log(outcome)
    checkBattle();

    function checkBattle() {
        for (let i = 0; i < allChoices.length; i++) {
            allChoices[i].style.zIndex = 1;
        }
        if (choice === 'rock') {
            if (randomChoice === 'paper') {
               const battle = document.getElementById('rockpaper');
                document.getElementById('hidden').style.display = 'flex';
                battle.style.zIndex = '11';
            }
            if (randomChoice === 'scissors') {
                const battle = document.getElementById('rockscissors');
                 document.getElementById('hidden').style.display = 'flex';
                 battle.style.zIndex = '10';
             }
             else {
                    const battle = document.getElementById('rockrock');
                     document.getElementById('hidden').style.display = 'flex';
                     battle.style.zIndex = '10';
             }
         }
         if (choice === 'paper') {
            if (randomChoice === 'rock') {
               const battle = document.getElementById('paperrock');
                document.getElementById('hidden').style.display = 'flex';
                battle.style.zIndex = '11';
            }
            if (randomChoice === 'scissors') {
                const battle = document.getElementById('paperscissors');
                 document.getElementById('hidden').style.display = 'flex';
                 battle.style.zIndex = '10';
             }
             else {
                    const battle = document.getElementById('paperpaper');
                     document.getElementById('hidden').style.display = 'flex';
                     battle.style.zIndex = '10';
             }
         }
         if (choice === 'scissors') {
            if (randomChoice === 'rock') {
               const battle = document.getElementById('scissorsrock');
                document.getElementById('hidden').style.display = 'flex';
                battle.style.zIndex = '11';
            }
            if (randomChoice === 'paper') {
                const battle = document.getElementById('scissorspaper');
                 document.getElementById('hidden').style.display = 'flex';
                 battle.style.zIndex = '10';
             }
             else {
                    const battle = document.getElementById('scissorsscissors');
                     document.getElementById('hidden').style.display = 'flex';
                     battle.style.zIndex = '10';
             }
         }
        }

    function decision() {    
        const win = document.getElementById('win');
        const lost = document.getElementById('lost');
        const draw = document.getElementById('draw');
        win.style.display = 'none';
        lost.style.display = 'none';
        draw.style.display = 'none';

        if (outcome === 'win') {
            win.style.display = 'block';
        }
        if (outcome === 'lost') {
            lost.style.display = 'block';
        }
        if (outcome === 'draw') {
            draw.style.display = 'block';
        } 
    }
    decision()    
}

chooseRock.addEventListener('click',jokenpo);
choosePaper.addEventListener('click',jokenpo);
chooseScissors.addEventListener('click',jokenpo);


// window.onclick = function() {
//     const hidden = document.getElementById('hidden');
//     hidden.style.display = "none";
// }

const hidden = document.getElementById('hidden');

window.onclick = function(event) {
    if (event.target == hidden) {
      hidden.style.display = "none";
    }
  }

  hidden.onclick = function() {
    hidden.style.display = "none";
    document.getElementById('win').style.display = 'none';
    document.getElementById('lost').style.display = 'none';
    document.getElementById('draw').style.display = 'none';
  }
